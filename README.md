# easy-ecommerce
This is an e-commerce platform that specializes in selling Indian and Nepali ethnic wear. Customers can browse through various categories of clothing items and purchase them online. The website provides a user-friendly interface, allowing customers to easily search for their desired products, add them to their cart, and complete the purchase process. The website also allows registered users to save their favorite items and receive notifications about discounts and new arrivals. Additionally, the website has an admin panel that allows the website owner to manage products, orders, and user data.

# To run this project,
go into the /client folder and run npm start

go into the /backend folder and run npm start

Add appropriate secret and access keys in the client/.env and backend/.env files. 

