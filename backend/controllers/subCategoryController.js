const SubCategory = require('../models/subCategoryModel')
const Category = require('../models/categoryModel')
const slugify = require("slugify");
const AWS = require('aws-sdk');
const multer = require('multer');
require('dotenv').config()

const storage = multer.memoryStorage({
    destination: function(req,file, cb){
        cb(null, '')
    }
})

const filefilter = (req,file,cb) => {
    if(file.mimeType === 'image/jpeg' || file.mimetype === 'image/jpg'){
        cb(null, true)
    }else{
        cb(null, false)
    }
}

const upload = (req, res, next) => {
    multer({storage:storage, fileFilter: filefilter}).single('image');
    next();
}

const s3 = new AWS.S3({
    credentials : {
        accessKeyId: process.env.S3_ACCESS_KEY,
        secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
        region: process.env.S3_BUCKET_REGION,
    }
})

const createSubCategories = async (req, res) => {
    const name = req.body.name;
    const category = req.body.category;

    // Check if subcategory name and category are provided
    if (!name || !category) {
        return res.status(400).send({ message: "Both subcategory name and category are required." });
    }

    const slug = slugify(name.toLowerCase());

    const saveSubCategory = (imageURL) => {
        // Create the subcategory
        const subcategory = new SubCategory({
            name,
            slug,
            image: imageURL,
            category
        });

        subcategory.save()
            .then(result => {
                res.status(200).send({
                    _id: result._id,
                    name: result.name,
                    slug: result.slug,
                    image: imageURL,
                    category: result.category
                })
            })
            .catch(err => {
                res.send({ message: err })
            })
    }

    // Check if an image is provided
    if (req.files && req.files.image) {
        const uploadParams = {
            Bucket: 'subcategories-image-upload-aws',
            Key: req.files.image.name,
            Body: Buffer.from(req.files.image.data),
            ACL: 'public-read-write',
            ContentType: "image/jpeg",
        }

        s3.upload(uploadParams, function (err, data) {
            console.log(data);
            saveSubCategory(data.Location);
        });
    } else {
        saveSubCategory(null);
    }
}


const updateSubCategory = async (req, res) => {
    const name = req.body.name;

    console.log(req.files);

    // check if image exists before uploading
    let imageLocation = "";
    if (req.files && req.files.image) {
        const uploadParams = {
            Bucket: "subcategories-image-upload-aws",
            Key: req.files.image.name,
            Body: Buffer.from(req.files.image.data),
            ACL: "public-read-write",
            ContentType: "image/jpeg",
        };

        s3.upload(uploadParams, function (err, data) {
            if (err) {
                console.log(err);
                return res.status(400).send({ message: err });
            }
            console.log(data);
            imageLocation = data.Location;
        });
    }

    const slug = slugify(name.toLowerCase());
    // Update the subcategory
    const subcategory = await SubCategory.findByIdAndUpdate(
        req.params.id,
        {
            name,
            slug,
            category: req.body.category,
            ...(imageLocation && { image: imageLocation }),
        },
        { new: true }
    );

    subcategory
        .save()
        .then((result) => {
            res.status(200).send({
                _id: result._id,
                name: result.name,
                slug: result.slug,
                image: result.image || "",
                category: result.category,
            });
        })
        .catch((err) => {
            res.send({ message: err });
        });
};

const deleteSubCategory = async (req, res) => {
    try{
        const subcategoryId = req.params.id;
        const subcategory = await SubCategory.findById(subcategoryId);
        if (!subcategory) {
            return res.status(404).json({ message: "Subcategory not found." });
        }
        // delete the image from S3 if it exists
        if (subcategory.image) {
            const deleteParams = {
                Bucket: "subcategories-image-upload-aws",
                Key: subcategory.image.split("/").pop(), // get the object key from the image URL
            };
            s3.deleteObject(deleteParams, (err, data) => {
                if (err) {
                    console.log(err);
                }
                console.log(`Deleted image: ${subcategory.image}`);
            });
        }
        const deletedSubCategory = await SubCategory.findByIdAndRemove(subcategoryId);
        if (!deletedSubCategory) {
            return res.status(404).json({ message: 'Subcategory not found.' });
        }
        res.status(200).json(deletedSubCategory);
    } catch (err) {
        return res.status(400).json(err)
    }
}

const getAllSubCategories = async (req, res) => {
    try {
        const subcategories = await SubCategory.find().populate("category").sort({ createdAt: -1 });
        res.status(200).json(subcategories);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Server Error' });
    }
};

const getAllSubCategoriesByCategoryName = async(req,res) => {
    const {categoryName} = req.params;
    try{
        const category = await Category.findOne({ slug: categoryName });
        if (!category) {
            return res.status(404).json({ error: 'Category not found' });
        }
        const subCategories = await SubCategory.find({category: category._id}).populate("category").sort({ createdAt: -1 });
        res.status(200).json({subCategories});
    } catch(error){
        res.status(500).json({error: error.message});
    }
}

const getAllSubCategoriesByCategoryId = async(req,res) => {
    const {categoryId} = req.params;
    try{
        const category = await Category.findOne({ _id: categoryId });
        if (!category) {
            return res.status(404).json({ error: 'Category not found' });
        }
        const subCategories = await SubCategory.find({category: category._id}).populate("category").sort({ createdAt: -1 });
        res.status(200).json({subCategories});
    } catch(error){
        res.status(500).json({error: error.message});
    }
}

const getSubCategoryByCategoryName = async(req,res) => {
    const {categoryName, slug} = req.params;
    try{
        const category = await Category.findOne({ slug: categoryName });
        if (!category) {
            return res.status(404).json({ error: 'Category not found' });
        }
        const subCategory = await SubCategory.findOne({category: category._id, slug}).populate("category").sort({ createdAt: -1 });

        if(!subCategory){
            return res.status(404).json({ message: 'Subcategory not found'})
        }
        res.status(200).json({subCategory});
    } catch(error){
        console.log(error);
        res.status(500).json({error: error.message});
    }
}

const singleSubCategory = async (req, res) => {
    try{
        const subcategory = await SubCategory.findOne({ slug: req.params.slug })
        res.json(subcategory)
    } catch (err) {
        return res.json.status(400).json(err)
    }
}

module.exports = {
    createSubCategories,
    updateSubCategory,
    deleteSubCategory,
    getAllSubCategories,
    singleSubCategory,
    upload,
    getAllSubCategoriesByCategoryName,
    getAllSubCategoriesByCategoryId,
    getSubCategoryByCategoryName
}
