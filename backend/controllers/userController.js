const User = require("../models/userModel");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const sgMail = require("@sendgrid/mail");
const Order = require("../models/orders");
const Product = require("../models/productModel");
require("dotenv").config();
sgMail.setApiKey(process.env.SENDGRID_KEY);

const createUser = async (req, res) => {
  try {
    const {
      firstName,
      lastName,
      email,
      streetAddress1,
      streetAddress2,
      city,
      state,
      zipCode,
      phoneNumber,
      password,
    } = req.body;
    const newUser = new User({
      name: {
        first: firstName,
        last: lastName,
      },
      email: email,
      address: {
        street1: streetAddress1,
        street2: streetAddress2,
        city: city,
        state: state,
        zip: zipCode,
      },
      phone: phoneNumber,
      password: password,
    });
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.json({ err: "User already exists." });
    }
    const savedUser = await newUser.save();

    //prepare email
    const emailData = {
      from: process.env.EMAIL_FROM,
      to: savedUser.email,
      subject: `Welcome to Kanchan's Choice!`,
      html: `
      <h4>Hi ${savedUser.name.first}, </h4><p>You have successfully with signed up with
      Kanchan's Choice.</p>
      <p>Visit Kanchan's Choice for more details. Happy Shopping!</p>
      `,
    };

    //send email
    try {
      await sgMail.send(emailData);
    } catch (err) {
      console.log(err);
    }

    res.status(201).json(savedUser);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    const id = { id: user._id };
    const currentUser = {
      id: user._id,
      name: {
        first: user.name.first,
        last: user.name.last,
      },
      email: user.name.email,
      address: {
        street1: user.address.street1,
        street2: user.address.street2,
        city: user.address.city,
        state: user.address.state,
        zip: user.address.zip,
      },
      phone: user.phone,
    };
    const token = await jwt.sign(id, process.env.JWT_SECRET, {
      expiresIn: "5h",
    });

    if (user.role === 0) {
      return res.json({ currentUser, token });
    } else {
      return res.redirect("/admin");
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

const loginAdmin = async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(401).json({ message: "Invalid email or password" });
    }

    const id = { id: user._id };
    const currentUser = {
      id: user._id,
      name: {
        first: user.name.first,
        last: user.name.last,
      },
      email: user.name.email,
      address: {
        street1: user.address.street1,
        street2: user.address.street2,
        city: user.address.city,
        state: user.address.state,
        zip: user.address.zip,
      },
      phone: user.phone,
    };
    const token = await jwt.sign(id, process.env.JWT_SECRET, {
      expiresIn: "5h",
    });

    if (user.role === 1) {
      return res.json({ currentUser, token });
    } else {
      return res.redirect("/login");
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// made for testing purpose
const secret = async (req, res) => {
  res.json({ currentUser: req.user });
};

const getUsers = async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

const getUserById = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    res.json(user);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

const updateUser = async (req, res) => {
  try {
    const {
      firstName,
      lastName,
      email,
      street1,
      street2,
      city,
      originalState,
      zip,
      phone,
      password,
      currPassword,
    } = req.body;
    const user = await User.findById(req.user.id);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = password
      ? await bcrypt.hash(password, salt)
      : undefined;
    if (currPassword) {
      const passwordMatch = await bcrypt.compare(currPassword, user.password);
      if (!passwordMatch) {
        return res.status(401).json({ error: "Current password is incorrect" });
      }
    }
    const updated = await User.findByIdAndUpdate(
      req.user.id,
      {
        name: {
          first: firstName || user.name.first,
          last: lastName || user.name.last,
        },
        email: email,
        address: {
          street1: street1 || user.address.street1,
          street2: street2 || user.address.street2,
          city: city,
          state: originalState || user.address.state,
          zip: zip || user.address.zipCode,
        },
        phone: phone || user.address.phoneNumber,
        password: hashedPassword || user.password,
      },
      { new: true }
    );
    updated.password = undefined;
    res.status(200).json(updated);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

const deleteUser = async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    res.sendStatus(204);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

const verified = async (req, res) => {
  res.json({ ok: true });
};

const getOrders = async (req, res) => {
  try {
    const orders = await Order.find({ buyer: req.params.id })
      .sort({ createdAt: "desc" })
      .populate("products.product");
    res.json(orders);
  } catch (err) {
    console.log(err);
  }
};

const allOrders = async (req, res) => {
  try {
    const orders = await Order.find()
      .populate("products.product")
      .populate("buyer");

    res.json(orders);
  } catch (err) {
    console.log(err);
  }
};

const allEmail = async (req, res) => {
  try {
    const users = await User.find().populate("email");
    const emails = users.map((user) => user.email);
    const { subject, message } = req.body;

    for (let i = 0; i < emails.length; i++) {
      const emailData = {
        from: process.env.EMAIL_FROM,
        to: emails[i],
        subject: `${subject}`,
        html: `
              <p> Dear Customer, </p>

              <p> ${message} </p>
              `,
      };
      try {
        sgMail.send(emailData);
      } catch (err) {
        console.log(err);
      }
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  createUser,
  loginUser,
  loginAdmin,
  secret,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
  verified,
  getOrders,
  allOrders,
  allEmail,
};
