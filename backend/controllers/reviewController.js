const Reviews = require("../models/reviewModel");

const createReview = async (req, res) => {
    try {
        const { rating, comment } = req.body;
        const review = await Reviews.create({ rating, comment, date: new Date() });
        res.status(201).json({ status: "success", data: review });
    } catch (error) {
        console.error(error);
        res.status(500).json({ status: "error", message: "Failed to create review" });
    }
};

const getAllReviews = async (req, res) => {
    try {
        const reviews = await Reviews.find();
        res.status(200).json({ status: "success", data: reviews });
    } catch (error) {
        console.error(error);
        res.status(500).json({ status: "error", message: "Failed to get reviews" });
    }
};

module.exports = {
    createReview,
    getAllReviews
};
