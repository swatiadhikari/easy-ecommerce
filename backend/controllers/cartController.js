const Cart = require("../models/cartModel");

// add product to cart
const updateCart = async (req, res) => {
  const { userId, cart } = req.body;

  try {
    let existingCart = await Cart.findOne({ userId });

    if (!existingCart) {
      existingCart = new Cart({ userId, cart }); // Create new cart object with user id and cart array
      await existingCart.save();
    } else {
      existingCart.products = cart;
      await existingCart.save();
    }

    res.json(existingCart);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

// get all products in user's cart
const getCart = async (req, res) => {
  const userId = req.user.id;
  try {
    const existingCart = await Cart.findOne({ userId });
    if (!existingCart) {
      return res.json(existingCart.products);
    }
    res.json(existingCart.products);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

const checkout = async (req, res) => {
  try {
    res.status(201).send("Welcome to Checkout Page");
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  updateCart,
  getCart,
  checkout,
};
