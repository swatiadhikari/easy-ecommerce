const Product = require("../models/productModel");
const SubCategory = require("../models/subCategoryModel");
const Category = require("../models/categoryModel");
const User = require("../models/userModel");
const slugify = require("slugify");
const AWS = require("aws-sdk");
const multer = require("multer");
require("dotenv").config();
const braintree = require("braintree");
const dotenv = require("dotenv");
const Order = require("../models/orders");
const sgMail = require("@sendgrid/mail");
dotenv.config();
sgMail.setApiKey(process.env.SENDGRID_KEY);
const gateway = new braintree.BraintreeGateway({
  environment: braintree.Environment.Sandbox,
  merchantId: process.env.BRAINTREE_MERCHANT_ID,
  publicKey: process.env.BRAINTREE_PUBLIC_KEY,
  privateKey: process.env.BRAINTREE_PRIVATE_KEY,
});

const storage = multer.memoryStorage({
  destination: function (req, file, cb) {
    cb(null, "");
  },
});

const filefilter = (req, file, cb) => {
  if (file.mimeType === "image/jpeg" || file.mimetype === "image/jpg") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = (req, res, next) => {
  multer({
    storage: storage,
    fileFilter: filefilter,
    limits: {
      fileSize: 1024 * 1024 * 5,
    },
  }).single("image");
  next();
};

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    region: process.env.S3_BUCKET_REGION,
  },
});

const createProduct = async (req, res) => {
  const {
    productName,
    productDescription,
    productPrice,
    salePrice,
    productCategory,
    productSize,
    subCategory,
    isFeatured,
  } = req.body;
  // Parse the productSize string to a JavaScript object
  const parsedProductSize = JSON.parse(productSize);
  console.log(parsedProductSize);
  // Validate required fields
  const requiredFields = ["productName", "productPrice", "productSize", "productCategory", "subCategory"];
  for (const field of requiredFields) {
    if (!req.body[field]) {
      return res.json({ error: `${field} is required` });
    }
  }
  // Upload images to S3
  const imageKeys = ["front", "left", "right", "back"];
  const uploadedImages = {};
  for (const key of imageKeys) {
    const imageFile = req.files && req.files[key];
    if (imageFile) {
      const uploadParams = {
        Bucket: "products-image-upload-aws",
        Key: imageFile.name,
        Body: imageFile.data,
        ACL: "public-read-write",
        ContentType: "image/jpeg",
      };

      const s3Data = await s3.upload(uploadParams).promise();
      uploadedImages[key] = s3Data.Location;
    }
  }

  // Create and save product
  const product = new Product({
    productName,
    slug: slugify(productName),
    productDescription,
    productPrice,
    salePrice,
    productCategory,
    subCategory,
    productSize,
    isFeatured,
    images: uploadedImages,
  });

  if (productSize) {
    const sizes = [];
    for (const { label, quantity } of parsedProductSize.sizes) {
      const qty = parseFloat(quantity);
      if (!isNaN(qty)) {
        const lbl = label.toString();
        sizes.push({ label: lbl, quantity: qty });
      }
    }
    product.productSize = { sizes };
  }
  try {
    const savedProduct = await product.save();
    return res.status(200).send(savedProduct);
  } catch (error) {
    return res.send({ message: error.message });
  }
};
const getAllProducts = async (req, res) => {
  try {
    const products = await Product.find({})
      .populate("productCategory")
      .populate("subCategory")
      .sort({ createdAt: -1 });
    res.status(200).json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server Error" });
  }
};

const getProductsBySubCategory = async (req, res) => {
  try {
    const { subcategoryId } = req.params;
    const subCategory = await SubCategory.findOne({ _id: subcategoryId });
    if (!subCategory) {
      return res.status(404).json({ message: "Subcategory not found" });
    }
    const products = await Product.find({ subCategory: subCategory._id });
    return res.status(200).json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};
const getProductsBySubCategoryAndCategory = async (req, res) => {
  try {
    const { categoryName, subCategorySlug } = req.params;
    const category = await Category.findOne({ slug: categoryName });
    if (!category) {
      return res.status(404).json({ message: "Category not found" });
    }
    const subCategory = await SubCategory.findOne({
      slug: subCategorySlug,
      category: category._id,
    });
    if (!subCategory) {
      return res.status(404).json({ message: "Subcategory not found" });
    }
    const products = await Product.find({ subCategory: subCategory._id });
    return res.status(200).json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

const singleProduct = async (req, res) => {
  const { productId } = req.params;
  try {
    const product = await Product.findById(productId).populate(
      "productCategory"
    );
    res.status(200).json(product);
  } catch (err) {
    console.log(err);
  }
};

const updateProduct = async (req, res) => {
  const { productId } = req.params;
  const {
    productName,
    productDescription,
    productPrice,
    salePrice,
    productCategory,
    productSize,
    subCategory,
    isFeatured,
  } = req.body;
  // Parse the productSize string to a JavaScript object
  const parsedProductSize = JSON.parse(productSize);
  console.log(parsedProductSize);
  try {
    // Find the product by ID
    let product = await Product.findById(productId);
    if (!product) {
      return res.json({ error: `Product not found` });
    }
    // Update product fields
    product.productName = productName || product.productName;
    product.slug = slugify(productName) || product.slug;
    product.productDescription =
      productDescription || product.productDescription;
    product.productPrice = productPrice || product.productPrice;
    product.salePrice = salePrice || product.salePrice;
    product.productCategory = productCategory || product.productCategory;
    product.subCategory = subCategory || product.subCategory;
    product.isFeatured = isFeatured || product.isFeatured;

    if (productSize) {
      const sizes = [];
      for (const { label, quantity } of parsedProductSize.sizes) {
        const qty = parseFloat(quantity);
        if (!isNaN(qty)) {
          const lbl = label.toString();
          sizes.push({ label: lbl, quantity: qty });
        }
      }
      product.productSize = { sizes };
    }

    // Upload new images to S3
    const imageKeys = ["front", "left", "right", "back"];
    const uploadedImages = {};
    for (const key of imageKeys) {
      const imageFile = req.files && req.files[key];
      if (imageFile) {
        const uploadParams = {
          Bucket: "products-image-upload-aws",
          Key: imageFile.name,
          Body: imageFile.data,
          ACL: "public-read-write",
          ContentType: "image/jpeg",
        };
        const s3Data = await s3.upload(uploadParams).promise();
        uploadedImages[key] = s3Data.Location;
      } else {
        uploadedImages[key] = product.images && product.images[key]; // Add null check for product.images object
      }
    }
    // Update product images
    product.images = uploadedImages;

    console.log(product);

    // Save the updated product
    const savedProduct = await product.save({ new: true });
    console.log("saved products" + savedProduct);
    return res.status(200).send(savedProduct);
  } catch (error) {
    return res.send({ message: error.message });
  }
};

const deleteProduct = async (req, res) => {
  try {
    const product = await Product.findByIdAndDelete(req.params.productId);
    res.json(product);
  } catch (err) {
    console.log(err);
  }
};

const addFavorite = async (req, res) => {
  const { productId } = req.params;
  const userId = req.user.id;

  try {
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Check if the product is already in user's favorites
    const isFavorite = user.favorites.includes(productId);
    if (isFavorite) {
      return res.status(400).json({ message: "Product already in favorites" });
    }

    // Add the product to user's favorites
    user.favorites.push(productId);
    await user.save();

    res.json({
      message: "Product added to favorites",
      favorites: user.favorites,
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal server error" });
  }
};

const productsSearch = async (req, res) => {
  try {
    const { keyword } = req.params;
    const results = await Product.find({
      $or: [
        { productName: { $regex: keyword, $options: "i" } },
        { productDescription: { $regex: keyword, $options: "i" } },
      ],
    });

    res.json(results);
  } catch (err) {
    console.log(err);
  }
};

const relatedProducts = async (req, res) => {
  try {
    const { productId, categoryId } = req.params;
    const related = await Product.find({
      subCategory: categoryId,
      _id: { $ne: productId },
    })
      .populate("subCategory")
      .limit(6);

    res.json(related);
  } catch (err) {
    console.log(err);
  }
};

const featuredProducts = async (req, res) => {
  try {
    const featured = await Product.find({
      isFeatured: true,
    }).limit(8);
    res.json(featured);
  } catch (err) {
    console.log(err);
  }
};

const getToken = async (req, res) => {
  try {
    gateway.clientToken.generate({}, function (err, response) {
      if (err) {
        res.status(500).send(err);
      } else {
        res.send(response);
      }
    });
  } catch (err) {
    console.log(err);
  }
};

const processPayment = async (req, res) => {
  try {
    console.log(req.body);

    const { nonce, totalAmount, cart, userId } = req.body;

    let newTransaction = gateway.transaction.sale(
      {
        amount: totalAmount,
        paymentMethodNonce: nonce,
        options: {
          submitForSettlement: true,
        },
      },
      async function (error, result) {
        if (result) {
          //res.send(result);
          //create order
          const order = new Order({
            products: cart,
            payment: result,
            buyer: userId,
          }).save();
          //decrease the quantity of item
          decrementQuantity(cart);
          console.log(cart);
          const name = await User.findById(userId);
          console.log(name.email);

          const emailData = {
            from: process.env.EMAIL_FROM,
            to: name.email,
            subject: "Order Confirmation",
            html: `
             <p>Hi ${name.name.first} ,
             </p><p>Thank you for your order! We are currently processing your order and we will inform you via email when your product is shipped. In the meantime, here are the details of your purchase:</p><ul>${await Promise.all(
               cart.map(async (item) => {
                 const p = await Product.findById(item.product);
                 return `<li> <img src="${p.images.front}"  style="
                   width: 100px;
                   height: 100px;
                   objectFit: cover;
                   objectPosition: center;
                 " alt="p1"> </img> <br> ${p.productName} - Size: ${item.selectedSize} - Qty: ${item.quantity} - Price: $${item.price}</li><hr/> `;
               })
             )}
             </ul><p>Total Amount:<strong> $${totalAmount} </strong></p><p>Thank you for shopping with us! </p>
            `,
          };
          const emailDataAdmin = {
            from: process.env.EMAIL_FROM,
            to: process.env.EMAIL_FROM,
            subject: "New Order Placed",
            html: `
      
            <p> A new order has been placed, please open the admin panel to view it ! <p>
            
            `,
          };

          try {
            await sgMail.send(emailData);
            await sgMail.send(emailDataAdmin);
          } catch (err) {
            console.log(err);
          }
          res.json({ ok: true });
        } else {
          res.status(500).send(error);
        }
      }
    );
  } catch (err) {
    console.log(err);
  }
};

const decrementQuantity = async (cart) => {
  try {
    //build MongoDB query

    const bulkOps = cart.map((item) => {
      return {
        updateOne: {
          filter: {
            _id: item.product,
            "productSize.sizes.label": item.selectedSize,
          },
          update: {
            $inc: {
              "productSize.sizes.$.quantity": -item.quantity, // decrement the quantity by the sold amount
            },
          },
        },
      };
    });

    console.log("Bulkops", bulkOps);
    const updated = await Product.bulkWrite(bulkOps, {});
    console.log("Bulkupdated", updated);
  } catch (err) {
    console.log(err);
  }
};

const orderStatus = async (req, res) => {
  try {
    const { orderId } = req.params;
    const { status } = req.body;
    const order = await Order.findByIdAndUpdate(
      orderId,
      {
        status: status,
      },
      { new: true }
    ).populate("buyer");
    const emailData = {
      from: process.env.EMAIL_FROM,
      to: order.buyer.email,
      subject: "Order Status",
      html: `
      <p> Hi ${order.buyer.name.first}, </p>

      <p>Your order status: <span style="color:green;"> ${order.status} </span><p>
      <p> Thank you for shopping with Kanchans Choice. Please take a moment to leave us a review: <a href="${process.env.CLIENT_URL}/review">Leave a Review</a> </p>
      
      `,
    };
    try {
      await sgMail.send(emailData);
    } catch (err) {
      console.log(err);
    }

    res.json(order);
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  createProduct,
  getAllProducts,
  singleProduct,
  updateProduct,
  deleteProduct,
  upload,
  getProductsBySubCategoryAndCategory,
  getProductsBySubCategory,
  addFavorite,
  productsSearch,
  relatedProducts,
  featuredProducts,
  getToken,
  processPayment,
  orderStatus,
};
