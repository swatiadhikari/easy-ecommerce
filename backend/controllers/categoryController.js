const Category = require('../models/categoryModel');
const slugify = require("slugify");

const createCategories = async (req, res) => {
    try{
        const name = req.body.name;
        if (!name.trim() ) {
            return res.status(401).json({err: "Name is required"})
        }
        const existingCategory = await Category.findOne({ name })
        if (existingCategory) {
            return res.json({err: "Category already exists"})
        }

        const slug = slugify(name);
        const newCategory = new Category({
            name,
            slug
        })
        await newCategory.save();
        res.json(newCategory)
    } catch (err) {
        return res.status(400).json({err})
    }
}

const updateCategory = async (req, res) => {
    try{
        const { name } = req.body.name;
        const {categoryId} = req.params
        const slug = slugify(name);
        const category = await Category.findByIdAndUpdate(categoryId, {
            name,
            slug
        }, {new: true})

        await category.save();
        res.json(category)
    } catch (err) {
        return res.status(400).json(err)
    }
}

const  deleteCategory = async (req, res) => {
    try{
        const categoryId = req.params.id;
        const deletedCategory = await Category.findByIdAndRemove(categoryId);
        if (!deletedCategory) {
            return res.status(404).json({ message: 'Category not found.' });
        }
        res.status(200).json(deletedCategory);
    } catch (err) {
        return res.status(400).json(err)
    }
}

const getAllCategories = async (req, res) => {
    try {
        const categories = await Category.find({});
        res.status(200).json(categories);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Server Error' });
    }
};

const singleCategory = async (req, res) => {
    try{
        const category = await Category.findOne({ slug: req.params.slug })
        res.json(category)
    } catch (err) {
        return res.json.status(400).json(err)
    }
}


module.exports = {
    createCategories,
    updateCategory,
    deleteCategory,
    getAllCategories,
    singleCategory
}