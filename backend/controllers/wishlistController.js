const Wishlist = require("../models/wishlistModel");

// add product to wishlist
const addToWishlist = async (req, res) => {
  const { productId } = req.body;
  const userId = req.user.id;

  try {
    let wishlist = await Wishlist.findOne({ userId });

    if (!wishlist) {
      wishlist = new Wishlist({ userId: userId, products: [productId] }); // Add userId to new wishlist object
      await wishlist.save();
    } else {
      if (!wishlist.products.includes(productId)) {
        wishlist.products.push(productId);
        await wishlist.save();
      }
    }

    res.json(wishlist);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

// remove product from wishlist
const removeFromWishlist = async (req, res) => {
  const { productId } = req.params;
  const userId = req.user.id;

  try {
    let wishlist = await Wishlist.findOne({ userId });

    if (wishlist) {
      wishlist.products = wishlist.products.filter(
        (p) => p.toString() !== productId
      );
      await wishlist.save();
    }

    res.json(wishlist);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

// get all products in user's wishlist
const getWishlist = async (req, res) => {
  const userId = req.user.id;
  try {
    const wishlist = await Wishlist.findOne({ userId }).populate("products");
    if (!wishlist) {
      return res.json({ products: [] });
    }
    res.json(wishlist);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
};

module.exports = {
  addToWishlist,
  removeFromWishlist,
  getWishlist,
};
