const express = require('express');
const router = express.Router();
const {
    requireSignIn,
    isAdmin
} = require("../middlewares/userMiddleware");

const {
    createSubCategories,
    updateSubCategory,
    deleteSubCategory,
    getAllSubCategories,
    singleSubCategory,
    upload,
    getAllSubCategoriesByCategoryName,
    getAllSubCategoriesByCategoryId,
    getSubCategoryByCategoryName
} = require('../controllers/subCategoryController')

//API for the category
router.post('/subcategory', upload, createSubCategories)
router.put('/subcategory/:id', updateSubCategory)
router.delete('/subcategory/:id', deleteSubCategory)
router.get('/subcategories', getAllSubCategories)
router.get('/subcategory/:slug', singleSubCategory)
router.get( '/subcategories/:categoryId', getAllSubCategoriesByCategoryId)
router.get('/categories/:categoryName/subcategories', getAllSubCategoriesByCategoryName)
router.get('/categories/:categoryName/subcategory/:slug', getSubCategoryByCategoryName)

module.exports = router