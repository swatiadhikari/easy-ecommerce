const express = require("express");
const router = express.Router();
const wishlistController = require("../controllers/wishlistController");
const formidable = require("express-formidable");
const { requireSignIn, isCustomer } = require("../middlewares/userMiddleware");
const {
  addToWishlist,
  getWishlist,
  removeFromWishlist,
} = require("../controllers/wishlistController");
const { add } = require("nodemon/lib/rules");
// add product to wishlist
router.post("/wishlist/add", requireSignIn, addToWishlist);
router.get("/wishlist", requireSignIn, getWishlist);
// remove product from wishlist
router.delete("/wishlist/delete/:productId", requireSignIn, removeFromWishlist);

//testing is customer
router.get("/isCustomer", requireSignIn, isCustomer, (req, res) => {
  res.json({ message: "The user is customer" });
});
module.exports = router;
