const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const formidable = require("express-formidable");
const { requireSignIn } = require("../middlewares/userMiddleware");
const { updateCart, getCart, checkout } = cartController;
// add product to wishlist
router.post("/cart/update", requireSignIn, updateCart);
router.get("/cart", requireSignIn, getCart);
router.get("/cart/checkout", requireSignIn, checkout);

module.exports = router;
