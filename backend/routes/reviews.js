const express = require('express');
const router = express.Router();
const { requireSignIn } = require("../middlewares/userMiddleware");

const {
   createReview,
    getAllReviews
} = require('../controllers/reviewController')

router.post('/review', createReview)
router.get('/reviews', getAllReviews)

module.exports = router