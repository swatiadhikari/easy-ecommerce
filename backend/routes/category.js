const express = require('express');
const router = express.Router();
const formidable = require('express-formidable');
const {
    requireSignIn,
    isAdmin
} = require("../middlewares/userMiddleware");

const {
    createCategories,
    updateCategory,
    deleteCategory,
    getAllCategories,
    singleCategory,
} = require('../controllers/categoryController')

//API for the category
router.post('/category',requireSignIn, isAdmin, createCategories)
router.put('/category/:categoryId', requireSignIn, isAdmin, updateCategory)
router.delete('/category/:id', requireSignIn, isAdmin, deleteCategory)
router.get('/categories', getAllCategories)
router.get('/category/:slug', singleCategory)

module.exports = router