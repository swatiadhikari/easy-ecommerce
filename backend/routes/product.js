const express = require("express");
const formidable = require("express-formidable");
const router = express.Router();
const { requireSignIn, isAdmin } = require("../middlewares/userMiddleware");

const {
  createProduct,
  updateProduct,
  deleteProduct,
  getAllProducts,
  singleProduct,
  upload,
  getProductsBySubCategoryAndCategory,
  getProductsBySubCategory,
  addFavorite,
  productsSearch,
  relatedProducts,
  featuredProducts,
  getToken,
  processPayment,
  orderStatus,
} = require("../controllers/productController");

router.post("/product", upload, createProduct);
router.get("/products", getAllProducts);
router.get("/products/:subcategoryId", getProductsBySubCategory);
router.get(
  "/categories/:categoryName/subcategory/:subCategorySlug/products",
  getProductsBySubCategoryAndCategory
);
router.get("/product/:productId", singleProduct);
router.delete("/product/:productId", deleteProduct);
router.put("/product/:productId", updateProduct);
router.post("/product/favorite/:productId", addFavorite);
router.get("/products/search/:keyword", productsSearch);
router.get("/related-products/:productId/:categoryId", relatedProducts);
router.get("/featured-product", featuredProducts);
router.get("/braintree/token", getToken);
router.post("/braintree/payment", requireSignIn, processPayment);

router.put("/order-status/:orderId", requireSignIn, isAdmin, orderStatus);

module.exports = router;
