const express = require("express");
const router = express.Router();
const {
  createUser,
  loginUser,
  secret,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
  verified,
  loginAdmin,
  getOrders,
  allOrders,
  allEmail,
} = require("../controllers/userController");
const { requireSignIn, isAdmin, isCustomer} = require("../middlewares/userMiddleware");

router.post("/signup", createUser);
router.post("/login", loginUser);
router.post("/admin", loginAdmin);

// only for testing login is required features
router.get("/secret", isCustomer, verified);

//can be used in the future (these paths are giving errors when trying to fetch all the products, categories, and subcategories)
// router.get('/', getUsers);
//router.get('/:id',requireSignIn, getUserById);

router.put("/user", requireSignIn, updateUser);
router.delete("/:id", deleteUser);

//authenticate users as admin and requireSignIn
router.get("/auth-Check", requireSignIn, verified);
router.get("/admin-check", requireSignIn, isAdmin, verified);
router.get("/customer-check", isCustomer, verified);

router.get("/orders/:id", requireSignIn, getOrders);

//get all orders for admin
router.get("/all-orders", requireSignIn, isAdmin, allOrders);
router.get("/past-all-orders", requireSignIn, isAdmin, allOrders);

//send email to all
router.post("/send", requireSignIn, isAdmin, allEmail);

module.exports = router;
