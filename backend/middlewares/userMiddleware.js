const jwt = require('jsonwebtoken')
const User = require('../models/userModel')
const requireSignIn = (req, res, next) => {
    try {
        const decode = jwt.verify(
            req.headers.authorization,
            process.env.JWT_SECRET
        );
        req.user = decode
        next();
    } catch (err){
        return res.status(401).json(err)
    }
}
const isCustomer = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        if (!token) {
            return next();
        }
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        req.user = decoded;
        const user = await User.findById(decoded.id);
        if (user.role === 1) {
            return res.status(403).json({ ok: false });
        }
        next();
    } catch (err) {
        if (err.name === 'JsonWebTokenError') {
            return res.status(401).json({ error: 'Invalid token.' });
        }
        console.log(err);
        return res.status(500).json({ error: 'Server error.' });
    }
};



const isAdmin = async (req, res, next) => {
    try {
        const user = await User.findById(req.user.id);
        if (user.role !== 1) {
            return res.status(403).json({error: 'Access denied. Admin resource.'});
        } else {
            next();
        }
    } catch (err) {
        console.log(err)
    }
};

module.exports = {
    requireSignIn,
    isCustomer,
    isAdmin,
};
