const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const wishlistSchema = new mongoose.Schema({
  userId: {
    type: ObjectId,
    ref: "User",
    required: true,
  },
  products: [
    {
      type: ObjectId,
      ref: "Product",
      required: true,
    },
  ],
});

module.exports = mongoose.model("Wishlist", wishlistSchema);
