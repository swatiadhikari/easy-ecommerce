const mongoose = require("mongoose");

const reviewSchema = new mongoose.Schema(
    {
        rating: {
            type: Number,
            min: 1,
            max: 5,
            required: true
        },
        comment: {
            type: String,
        },
        date: {
            type: Date,
        }
    }
);

module.exports = mongoose.model("Reviews", reviewSchema);
