const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const productSchema = new mongoose.Schema(
  {
    productName: {
      type: String,
      trim: true,
      required: true,
      maxLength: 160,
    },
    slug: {
      type: String,
      unique: true,
      lowercase: true,
    },
    productDescription: {
      type: String,
      maxLength: 2000,
    },
    productCategory: {
      type: ObjectId,
      ref: "Category",
      required: true,
    },
    subCategory: {
      type: ObjectId,
      ref: "SubCategory",
      required: false,
    },
    productSize: {
      sizes: [
        {
          label: {
            type: String,
            required: true,
          },
          quantity: {
            type: Number,
            required: true,
          },
        },
      ],
    },
    productPrice: {
      type: Number,
      trim: true,
      required: true,
    },
    salePrice: {
      type: Number,
    },
    sold: {
      type: Number,
      default: 0,
    },
    shipping: {
      required: false,
      type: Boolean,
    },
    isFeatured: {
      type: Boolean,
    },
    images: {
      front: String,
      left: String,
      right: String,
      back: String,
    },
    favorites: [
      {
        type: ObjectId,
        ref: "User",
      },
    ],
  },
  { timestamps: true }
);

module.exports = mongoose.model("Product", productSchema);
