const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema;

const subCategorySchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        maxLength: 32,
        unique: true
    },
    slug: {
        type: String,
        unique: true,
        lower: true
    },
    image:{
       type: String
    },
    category:{
        type: ObjectId,
        ref: "Category",
        required: true,
    }
})

module.exports = mongoose.model('SubCategory', subCategorySchema);