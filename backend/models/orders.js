const mongoose = require("mongoose");

const { ObjectId } = mongoose.Schema;
const User = require("./userModel");

const orderSchema = new mongoose.Schema(
  {
    products: [
      {
        product: { type: ObjectId, ref: "Product" },
        selectedSize: String,
        quantity: Number,
        price: Number,
      },
    ],
    payment: {},
    buyer: { type: ObjectId, ref: "User" },
    status: {
      type: String,
      default: "Order Received",
      enum: ["Order Received", "Shipped", "Canceled"],
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Order", orderSchema);
