import React, { useState, useEffect } from "react";
import Header from "../../components/header/Header";
import { useCart } from "../../context/cart";
import Footer from "../../components/footer/Footer";
import { useAuth } from "../../context/auth";
import "../../css/CartPage.css";
import { Link, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import { toast } from "react-hot-toast";

const CartPage = () => {
  const { cart, clearCart, getCartTotal, decrementItem } = useCart();
  const [auth] = useAuth();
  const isLoggedIn = !!auth.token;
  const [productList, setProductList] = useState([]);
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    getProductList();
  }, [cart]);

  const getProductList = async () => {
    try {
      const promises = cart.map(async (item) => {
        const res = await axios.get(
          `http://localhost:4000/api/product/${item.product}`
        );
        const product = res.data;
        product.quantity = item.quantity;
        product.selectedSize = item.selectedSize;
        return product;
      });
      const products = await Promise.all(promises);
      setProductList(products);
    } catch (err) {
      console.error(err.message);
    }
  };

  const handleDecrementItem = (item, size) => {
    decrementItem(item, size);
    toast.success("Removed From Cart");
  };

  return (
    <>
      <Header></Header>
      <div className="container mt-5">
        {cart.length > 0 ? (
          <>
            <div className="d-flex flex-column ">
              <div className="d-flex justify-content-center">
                <h1 style={{ marginTop: "15px", fontWeight: "450" }}>
                  My Cart
                </h1>
              </div>
              <hr />
              {isLoggedIn ? (
                <div className="d-flex justify-content-end">
                  <button className="btn clear-cart" onClick={clearCart}>
                    Clear Cart
                  </button>
                </div>
              ) : (
                <></>
              )}
            </div>
            <div className="d-flex flex-column">
              {isLoggedIn ? (
                <>
                  <div className="d-flex flex-column justify-content-center ">
                    {productList.map((item, index) => (
                      <div
                        key={index}
                        className="d-flex flex-row cart-item justify-content-center"
                      >
                        <div className="cart-image-container">
                          <Link to={`/products/${item._id}`}>
                            <img
                              src={item.images.front}
                              alt={item.productName}
                              className="cart-image img-fluid"
                            />
                          </Link>
                        </div>
                        <div className="d-flex flex-column product-desc-container">
                          <h5 style={{ color: "#002e6a" }}>
                            {item.productName}
                          </h5>
                          <span>Size: {item.selectedSize}</span>
                          <span className="quant">Qty: {item.quantity}</span>
                          <span>Price: ${item.productPrice}</span>
                          <span>
                            Sub total: ${item.quantity * item.productPrice}
                          </span>
                        </div>
                        <div className="d-flex align-items-center">
                          <button
                            className="btn"
                            onClick={() =>
                              handleDecrementItem(item, item.selectedSize)
                            }
                          >
                            <i className="fa-solid fa-trash"></i>
                          </button>
                        </div>
                        <hr />
                      </div>
                    ))}
                  </div>
                  <hr />
                  <div className="d-flex flex-column mb-5 p-3">
                    <div className="d-flex justify-content-center">
                      <h2
                        style={{
                          marginBottom: "15px",
                          fontWeight: "400",
                          color: "#002e6a",
                        }}
                      >
                        Grand total:{" "}
                        <h8 style={{ color: "#C70044", fontWeight: "450" }}>
                          ${getCartTotal()}
                        </h8>
                      </h2>
                    </div>

                    <div
                      className="d-flex flex-row  justify-content-center"
                      style={{ marginBottom: "10px" }}
                    >
                      <Link to={"/cart/checkout"}>
                        <button
                          type="button"
                          className="btn button add-to-cart"
                        >
                          Checkout
                        </button>
                      </Link>
                    </div>
                    <Link to={"/"}>
                      <h8 className="d-flex justify-content-center mt-3">
                        {" "}
                        Continue Shopping
                      </h8>
                    </Link>
                  </div>
                </>
              ) : (
                <div className="continer">
                  <div className="d-flex justify-content-center">
                    <p>Please log in to view the contents of your cart</p>
                  </div>
                </div>
              )}
            </div>
          </>
        ) : (
          <div
            className="d-flex justify-content-center align-items-center"
            style={{ height: "70vh" }}
          >
            <div className="col">
              <div className="d-flex justify-content-center">
                <h1 className="p-5">Empty Cart</h1>
              </div>
              <div className="d-flex justify-content-center">
                <button
                  className="btn add-to-cart"
                  onClick={() =>
                    navigate(location.state || "/", { replace: true })
                  }
                >
                  Continue Shopping
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
      <Footer></Footer>
    </>
  );
};

export default CartPage;
