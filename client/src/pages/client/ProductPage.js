import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import axios from "axios";
import "../../css/ProductPage.css";
import { useAuth } from "../../context/auth";
import { toast } from "react-hot-toast";

const ProductPage = () => {
  const [subCategory, setSubCategory] = useState([]);
  const [selectedSubCategory, setSelectedSubCategory] = useState("");
  const [products, setProducts] = useState([]);
  const { category, subcategory } = useParams();
  const [wishlist, setWishlist] = useState([]);
  const [auth] = useAuth();
  const [priceRange, setPriceRange] = useState("");
  const handlePriceRangeChange = (event) => {
    setPriceRange(event.target.value);
  };

  useEffect(() => {
    axios
      .get(`http://localhost:4000/api/categories/${category}/subcategories`)
      .then((response) => {
        setSubCategory(response.data.subCategories);
      })
      .catch((error) => {
        console.error(error);
      });
    setSelectedSubCategory(subcategory);
  }, [subcategory]);

  useEffect(() => {
    if (selectedSubCategory) {
      axios
        .get(
          `http://localhost:4000/api/categories/${category}/subcategory/${selectedSubCategory}/products`
        )
        .then((response) => {
          const filteredProducts = filterProductsByPrice(response.data);
          const filteredProductsQ = filterProductByQuantity(filteredProducts);
          setProducts(filteredProductsQ);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [selectedSubCategory, priceRange]);

  const filterProductByQuantity = (products) => {
    return products.filter((p) => {
      return p.productSize.sizes.some((size) => size.quantity > 0);
    });
  };
  const filterProductsByPrice = (products) => {
    if (priceRange === "LessThan50") {
      return products.filter((p) => p.productPrice < 50);
    } else if (priceRange === "50to100") {
      return products.filter(
        (p) => p.productPrice >= 50 && p.productPrice <= 100
      );
    } else if (priceRange === "MoreThan100") {
      return products.filter((p) => p.productPrice > 100);
    } else {
      return products;
    }
  };

  const fetchWishlist = async () => {
    try {
      const userId = auth.currentUser.id;
      if (userId) {
        const response = await axios.get(`http://localhost:4000/api/wishlist`);
        setWishlist(response.data.products);
      }
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    fetchWishlist();
  }, [auth.currentUser.id]);
  const isInWishlist = (productId) => {
    return wishlist.some((item) => item._id === productId);
  };

  const addToWishlist = async (productId) => {
    try {
      fetchWishlist();
      const response = await axios.post(
        `http://localhost:4000/api/wishlist/add`,
        {
          productId,
        }
      );
      setWishlist([...wishlist, response.data.products]);
      toast.success("Added to Wishlist");
      await fetchWishlist();
    } catch (error) {
      console.error(error);
    }
  };
  const removeFromWishlist = async (productId) => {
    try {
      if (!auth) {
        return;
      }
      await axios.delete(
        `http://localhost:4000/api/wishlist/delete/${productId}`
      );
      const updatedWishlist = wishlist.filter(
        (products) => products._id !== productId
      );
      setWishlist(updatedWishlist);
      toast.success("Removed from Wishlist");
      await fetchWishlist();
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <>
      <Header />
      <div className="container mt-5 box">
        <div className="row mb-3">
          <div className="col-5 col-md-2 col-lg-2 ">
            <select
              className="form-select dropdown"
              id="productType"
              value={selectedSubCategory}
              onChange={(event) => setSelectedSubCategory(event.target.value)}
            >
              {subCategory.map((data) => (
                <option key={data.name} value={data.slug}>
                  {data.name}
                </option>
              ))}
            </select>
          </div>

          <div
            className="col-6 col-md-2 col-lg-2"
            style={{ marginLeft: "25px" }}
          >
            <select
              className="form-select"
              aria-label="Filter by Price"
              value={priceRange}
              onChange={handlePriceRangeChange}
            >
              <option value="">All Prices</option>
              <option value="LessThan50">Less than $50</option>
              <option value="50to100">$50 - $100</option>
              <option value="MoreThan100">$100 and more</option>
            </select>
          </div>
        </div>

        {/* <hr /> */}
        <div className="product-list row  row-cols-1 row-cols-md-4 g-4">
          {products.length > 0 &&
            products.map((product) => (
              <div className="product col-6 col-md-3" key={product.id}>
                <div className="card h-100">
                  <div className="position-relative">
                    <Link
                      to={`/products/${product._id}`}
                      className="text-decoration-none"
                    >
                      <img
                        src={product.images.front}
                        alt={product.name}
                        className="card-img-top img-fluid image-container"
                        // style={{ objectFit: 'cover', width: '100%', height: '100%' }}
                        style={{
                          Width: "100%",
                          objectFit: "cover",
                          objectPosition: "center center",
                        }}
                      />
                      {product.salePrice > 0 && (
                        <span className="off">
                          {Math.round(
                            ((product.productPrice - product.salePrice) /
                              product.productPrice) *
                              100
                          )}
                          % off
                        </span>
                      )}
                    </Link>
                    <button
                      className={`position-absolute bottom-0 end-0 btn ${
                        isInWishlist(product._id)
                          ? "heart-buttonSelected"
                          : "heart-buttonUnselected"
                      }`}
                      onClick={() => {
                        if (isInWishlist(product._id)) {
                          removeFromWishlist(product._id);
                        } else {
                          addToWishlist(product._id);
                        }
                      }}
                    >
                      <i className="fas fa-heart fa-2x"></i>
                    </button>
                  </div>

                  <div className="card-body d-flex flex-column justify-content-center align-items-center">
                    <Link
                      to={`/products/${product._id}`}
                      className="text-decoration-none"
                    >
                      <h4 className="card-title card-title-design mb-3 pb-0">
                        {product.productName}
                      </h4>
                    </Link>
                    <div class="divider" />
                    <div className="d-flex justify-content-between align-items-center w-100 pt-3">
                      <div>
                        {product.salePrice > 0 ? (
                          <p className="card-text priceTag flex-grow-0">
                            <span className="salePrice">
                              ${product.salePrice}
                            </span>
                            <span className="oldPrice">
                              ${product.productPrice}
                            </span>
                          </p>
                        ) : (
                          <p className="card-text priceTag flex-grow-0">
                            ${product.productPrice}
                          </p>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          {products.length === 0 && (
            <h3>Exciting new products are on their way !</h3>
          )}
        </div>
      </div>
      <Footer></Footer>
    </>
  );
};

export default ProductPage;
