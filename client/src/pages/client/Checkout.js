import React, { useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAuth } from "../../context/auth";
import { useCart } from "../../context/cart";

import "../../css/CategoryPage.css";
import axios from "axios";
import Footer from "../../components/footer/Footer";
import Header from "../../components/header/Header";
import DropIn from "braintree-web-drop-in-react";
import toast from "react-hot-toast";
import { Modal } from "react-bootstrap";

const Checkout = () => {
  const [clientToken, setClientToken] = useState("");
  const [instance, setInstance] = useState("");
  const [auth] = useAuth();
  const { cart, clearCart, getCartTotal } = useCart();
  const [showModal, setShowModal] = useState(false);

  const handleCloseModal = () => setShowModal(false);
  const handleShowModal = () => setShowModal(true);
  const totalAmount = getCartTotal();

  const navigate = useNavigate();
  const location = useLocation();
  const userId = auth.currentUser.id;
  useEffect(() => {
    if (auth.token) {
      getClientToken();
    }
  }, [auth.token]);

  const getClientToken = async () => {
    try {
      const { data } = await axios.get("/braintree/token");
      setClientToken(data.clientToken);
    } catch (err) {}
  };

  console.log(cart);
  const handleBuy = async (req, res) => {
    try {
      handleShowModal();

      const { nonce } = await instance.requestPaymentMethod();
      console.log("nonce => ", nonce);

      await axios.post("/braintree/payment", {
        nonce,
        totalAmount,
        cart,
        userId,
      });
      handleCloseModal();
      clearCart();
      navigate("/");
      toast.success("Payment Successful!");
    } catch (err) {
      handleCloseModal();

      console.log(err);
    }
  };

  const count = getCartTotal();
  return (
    <>
      {count > 0 ? (
        <>
          <Header></Header>

          <h1
            className="mt-4 text-center"
            style={{ color: "#002E6A", fontWeight: "400" }}
          >
            CheckOut
          </h1>
          <hr />
          <div className="container">
            <h2
              className="text-center mb-5"
              style={{ color: "#002E6A", fontWeight: "350" }}
            >
              Total amount is:{" "}
              <h8 style={{ color: "#C70044", fontWeight: "450" }}>
                {" "}
                ${totalAmount}{" "}
              </h8>
            </h2>
            <div className="row">
              <div className="col-md-6 mb-4">
                <div className="d-flex justify-content-center">
                  {!clientToken ? (
                    "Not Present"
                  ) : (
                    <DropIn
                      options={{
                        authorization: clientToken,
                        paypal: {
                          flow: "vault",
                        },
                        venmo: {
                          allowNewBrowserTab: false,
                        },
                      }}
                      onInstance={(instance) => setInstance(instance)}
                    />
                  )}
                </div>
                <button
                  onClick={handleBuy}
                  className="btn button w-100 mt-4"
                  disabled={!instance}
                >
                  Buy
                </button>
                <Modal
                  show={showModal}
                  onHide={handleCloseModal}
                  backdrop="static"
                  keyboard={false}
                >
                  <Modal.Header>
                    <Modal.Title>Processing payment...</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    Please wait while we process your payment.Do not refresh the
                    page or close the browser window.
                  </Modal.Body>
                </Modal>
              </div>
              <div className="col-md-6 d-flex justify-content-center">
                <div className="mt-5 mb-2">
                  <h2
                    style={{ color: "#C70044", fontWeight: "300" }}
                    className="ml-3"
                  >
                    Delivery Address:
                  </h2>
                  <h5>
                    {auth.currentUser.address.street1},{" "}
                    {auth.currentUser.address.street2}
                  </h5>
                  <h5>
                    {auth.currentUser.address.city},
                    {auth.currentUser.address.state},
                    {auth.currentUser.address.zip}
                  </h5>
                  <div className="d-flex justify-content-center mt-4">
                    <button
                      className="btn button w-60 btn-lg"
                      onClick={() =>
                        navigate("/profile", {
                          state: "/cart/checkout",
                        })
                      }
                    >
                      Change Address
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Footer></Footer>
        </>
      ) : (
        <>{navigate(location.state || "/", { replace: true })}</>
      )}
    </>
  );
};

export default Checkout;
