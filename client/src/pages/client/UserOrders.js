import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "../../css/CategoryPage.css";
import axios from "axios";
import Footer from "../../components/footer/Footer";
import Header from "../../components/header/Header";
import { useAuth } from "../../context/auth";

const CategoryPage = () => {
  const [auth] = useAuth();

  const [orders, setOrders] = useState([]);
  const userId = auth.currentUser.id;
  useEffect(() => {
    if (auth.token) getOrders();
  }, [auth.token]);

  const getOrders = async () => {
    try {
      const { data } = await axios.get(`/orders/${userId}`);
      console.log(data);
      setOrders(data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <Header></Header>

      <div className="container mb-4">
        <h1
          className="mt-5"
          style={{
            color: "#002E6A",
            marginTop: "15px",
            marginLeft: "110px",
            fontWeight: "450",
          }}
        >
          {" "}
          My Orders{" "}
        </h1>
        <div>
          {orders.length === 0 ? (
            <h3 className="mt-5">You have no orders yet, start shopping ! </h3>
          ) : (
            orders.map((order) => (
              <div key={order._id} className="card mb-3">
                <div className="card-body">
                  <h5 className="card-text" style={{ color: "green" }}>
                    Status: {order.status}{" "}
                  </h5>
                  <div
                    style={{
                      fontSize: "14px",
                      color: "dark gray",
                      fontStyle: "italic",
                      marginTop: "1px",
                      marginBottom: "5px",
                    }}
                  >
                    {order.status === "Order Recieved"
                      ? "(We are prepairing to ship your order)"
                      : ""}
                  </div>

                  <p className="card-text">
                    Total: ${order.payment.transaction.amount}
                  </p>
                  <p className="card-text" style={{ fontWeight: "450" }}>
                    Date: {order.createdAt.substring(0, 10)}
                  </p>

                  <h6 className="card-subtitle mb-2 text-muted">Products:</h6>
                  <hr style={{ marginLeft: "29px" }} />
                  <ol>
                    {order.products.map((p) => (
                      <li key={p._id}>
                        <Link
                          to={`/products/${p.product._id}`}
                          className="text-decoration-none"
                        >
                          <img
                            src={p.product.images.front}
                            alt={p.product.productName}
                            width="90"
                            height="90"
                            style={{
                              objectFit: "contain",
                              marginRight: "10px",
                              display: "block",
                            }}
                          />
                        </Link>
                        <p style={{ fontWeight: "350" }}>
                          {" "}
                          {p.product.productName} | Size: {p.selectedSize} |
                          Qty: {p.quantity} | Price:{" "}
                          <h10 style={{ fontWeight: "900px" }}>
                            ${p.product.productPrice}{" "}
                          </h10>
                        </p>
                        <hr />
                      </li>
                    ))}
                  </ol>
                </div>
              </div>
            ))
          )}
        </div>
      </div>
      <Footer></Footer>
    </>
  );
};

export default CategoryPage;
