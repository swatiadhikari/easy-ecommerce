import React from "react";
import { useAuth } from "../../context/auth";
import { Link } from "react-router-dom";
const AdminDashboardPage = () => {
  const [auth] = useAuth();

  return (
    <>
      <div className="container p-5">
        <div className="container-fluid mb-5">
          <h1>Hey {auth.currentUser.name.first}!</h1>
        </div>
        <div className="container-fluid">
          <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
            <div className="col">
              <Link to="/admin/orders" className="text-decoration-none">
                <div className="card bg-primary text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">New Orders</h3>
                </div>
              </Link>
            </div>
            <div className="col">
              <Link to="/admin/past-orders" className="text-decoration-none">
                <div className="card bg-primary text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">Past Orders</h3>
                </div>
              </Link>
            </div>
            {/*<div className="col">*/}
            {/*  <Link to="/analytics" className="text-decoration-none">*/}
            {/*    <div className="card bg-secondary text-white h-100 d-flex justify-content-center align-items-center">*/}
            {/*      <h3 className="card-title text-center">Analytics</h3>*/}
            {/*    </div>*/}
            {/*  </Link>*/}
            {/*</div>*/}
            <div className="col">
              <Link to="/admin/add" className="text-decoration-none">
                <div className="card bg-warning text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">Add Product</h3>
                </div>
              </Link>
            </div>
            <div className="col">
              <Link to="/admin/product/view" className="text-decoration-none">
                <div className="card bg-info text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">
                    View/Update Product
                  </h3>
                </div>
              </Link>
            </div>
            <div className="col">
              <Link to="/admin/subcategory/add" className="text-decoration-none">
                <div className="card bg-danger text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">Add Subcategory</h3>
                </div>
              </Link>
            </div>
            <div className="col">
              <Link
                to="/admin/subcategory/view"
                className="text-decoration-none"
              >
                <div className="card bg-dark text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">
                    View/Update Category
                  </h3>
                </div>
              </Link>
            </div>
            <div className="col">
              <Link
                to="/admin/carousel-images"
                className="text-decoration-none"
              >
                <div className="card bg-warning text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">Carousel Images</h3>
                </div>
              </Link>
            </div>
            <div className="col">
              <Link to="/admin/send-email" className="text-decoration-none">
                <div className="card bg-danger text-white h-100 d-flex justify-content-center align-items-center">
                  <h3 className="card-title text-center">Send Email</h3>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AdminDashboardPage;
