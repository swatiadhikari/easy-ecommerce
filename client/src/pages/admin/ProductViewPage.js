import React, { useEffect, useState } from "react";
import AdminHeader from "../../components/header/AdminHeader";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { NavLink } from "react-router-dom";
import "../../css/ProductAddPage.css";
import { toast } from "react-hot-toast";

const ProductViewPage = () => {
  const [getProductData, setProductData] = useState([]);
  const [categories, setCategories] = useState([]);
  const [subcategories, setSubcategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("");
  const [selectedSubcategory, setSelectedSubcategory] = useState("");

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    const getCategories = async () => {
      const res = await fetch("http://localhost:4000/api/categories", {
        method: "GET",
      });
      const data = await res.json();
      setCategories(data);
    };
    getCategories();
  }, []);

  const getSubcategories = async (categoryName) => {
    const res = await fetch(
      `http://localhost:4000/api/categories/${categoryName}/subcategories`,
      {
        method: "GET",
      }
    );
    const data = await res.json();
    setSubcategories(data.subCategories);
  };

  const getProducts = async (subcategoryName) => {
    if (selectedCategory && subcategoryName) {
      const res = await fetch(
        `http://localhost:4000/api/categories/${selectedCategory}/subcategory/${subcategoryName}/products`,
        {
          method: "GET",
        }
      );
      const data = await res.json();
      setProductData(data);
    }
  };

  const getData = async (e) => {
    try {
      const res = await fetch("http://localhost:4000/api/products", {
        method: "GET",
      });
      const data = await res.json();
      console.log(data);
      setProductData(data);
    } catch (err) {
      console.log("error");
    }
  };

  const handleCategoryChange = async (e) => {
    const categoryName = e.target.value;
    setSelectedCategory(categoryName);
    getSubcategories(categoryName);
  };

  const handleSubcategoryChange = (e) => {
    const subcategoryName = e.target.value;
    setSelectedSubcategory(subcategoryName);
    getProducts(subcategoryName);
  };

  useEffect(() => {
    getProducts();
  }, []);

  const deleteProduct = async (id) => {
    const confirmDelete = window.confirm(
      "Are you sure you want to delete this product?"
    );

    if (confirmDelete) {
      const res2 = await fetch(`http://localhost:4000/api/product/${id}`, {
        method: "DELETE",
      });

      const deleteProduct = await res2.json();
      console.log(deleteProduct);

      if (res2.status === 422 || !deleteProduct) {
        console.log("error");
      } else {
        console.log("Product Deleted");
        toast.success("Product Deleted!");
        getData();
      }
    }
  };

  return (
    <>
      <AdminHeader></AdminHeader>
      <div className="container mt-5 box">
        <div className="row mb-3">
          <div className="form-group">
            <label htmlFor="category">Category</label>
            <select
              className="form-control"
              style={{ width: "30%" }}
              id="category"
              onChange={handleCategoryChange}
              value={selectedCategory}
            >
              <option value="">Select a category</option>
              {categories.map((category) => (
                <option key={category.name} value={category.slug}>
                  {category.name}
                </option>
              ))}
            </select>
            {selectedCategory && (
              <div className="form-group">
                <label htmlFor="subcategory">Subcategory</label>
                <select
                  className="form-control"
                  style={{ width: "30%" }}
                  id="subcategory"
                  onChange={handleSubcategoryChange}
                  value={selectedSubcategory}
                >
                  <option value="">Select a subcategory</option>
                  {subcategories.map((subcategory) => (
                    <option key={subcategory.name} value={subcategory.slug}>
                      {subcategory.name}
                    </option>
                  ))}
                </select>
              </div>
            )}
          </div>

          <table className="table">
            <thead>
              <tr className="table-dark">
                <th scope="col">Image</th>
                <th scope="col">Product Name</th>
                <th scope="col">Update/Delete</th>
              </tr>
            </thead>
            <tbody>
              {Object.values(getProductData).map((element, index) => {
                const frontImage = element.images && element.images.front;
                return (
                  <tr key={element._id}>
                    <td>
                      <img
                        src={frontImage}
                        className="img-"
                        alt={element.productName}
                        style={{
                          width: "150px",
                          height: "150px",
                          objectFit: "contain",
                          objectPosition: "center",
                        }}
                      />
                    </td>
                    <td>{element.productName}</td>
                    <td key={index}>
                      <NavLink to={`/admin/product/update/${element._id}`}>
                        <button className="btn btn-primary">
                          <EditIcon />
                        </button>
                      </NavLink>
                      <button
                        className="btn btn-danger"
                        onClick={() => deleteProduct(element._id)}
                      >
                        <DeleteIcon />
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export default ProductViewPage;
