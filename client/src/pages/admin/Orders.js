import React, { useEffect, useState } from "react";
import "../../css/CategoryPage.css";
import axios from "axios";
import Footer from "../../components/footer/Footer";
import { toast } from "react-hot-toast";
import { useAuth } from "../../context/auth";
import AdminHeader from "../../components/header/AdminHeader";
import { Select } from "antd";

const { Option } = Select;

const AdminOrders = () => {
  const [auth] = useAuth();

  const [orders, setOrders] = useState([]);

  const [status] = useState(["Shipped", "Canceled"]);

  const [setChangeStatus] = useState("");

  useEffect(() => {
    if (auth.token) getOrders();
  }, [auth.token]);

  const getOrders = async () => {
    try {
      const { data } = await axios.get(`/all-orders`);
      setOrders(data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleChange = async (orderId, value) => {
    setChangeStatus(value);

    try {
      await axios.put(`/order-status/${orderId}`, {
        status: value,
      });
      toast.success("Order Shipped!");
      getOrders();
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <AdminHeader></AdminHeader>

      <div className="container mb-4">
        <h1
          className="mt-5"
          style={{
            color: "#002E6A",
            marginTop: "15px",
            marginLeft: "25%",
            fontWeight: "450",
          }}
        >
          {" "}
          New Orders{" "}
        </h1>
        <div>
          {orders.length === 0 ? (
            <h3 className="mt-5">There are no new orders ! </h3>
          ) : (
            orders.map(
              (order) =>
                order.status === "Order Received" && (
                  <div key={order._id} className="card mb-3">
                    <div className="card-body">
                      <h5 className="card-text" style={{ color: "green" }}>
                        <h7>Change Status: </h7>
                        <Select
                          className="col-6"
                          defaultValue={order.status}
                          onChange={(value) => handleChange(order._id, value)}
                        >
                          <option value="" disabled selected>
                            Select an option
                          </option>
                          {status.map((s, i) => (
                            <Option key={i} value={s}>
                              {" "}
                              {s}{" "}
                            </Option>
                          ))}
                        </Select>
                      </h5>

                      <p className="card-text">
                        Total: ${order.payment.transaction.amount}
                      </p>
                      <p className="card-text" style={{ fontWeight: "350" }}>
                        Date: {order.createdAt.substring(0, 10)}
                      </p>
                      <p>
                        Buyer: {order.buyer.name.first} {order.buyer.name.last}
                      </p>
                      <p className="card-text" style={{ fontWeight: "350" }}>
                        Email: {order.buyer.email}
                      </p>
                      <p className="card-text" style={{ fontWeight: "350" }}>
                        Address: {order.buyer.address.street1},{" "}
                        {order.buyer.address.street2},{" "}
                        {order.buyer.address.city}, {order.buyer.address.state}-
                        {order.buyer.address.zip}, USA
                      </p>

                      <h6 className="card-subtitle mb-2 text-muted">
                        Products:
                      </h6>
                      <h7> {order.products.buyer}</h7>
                      <hr style={{ marginLeft: "29px" }} />
                      <ol>
                        {order.products.map((p) => (
                          <li key={p._id}>
                            {p.product && (
                              <>
                                <img
                                  src={p.product.images.front}
                                  alt={p.product.productName}
                                  width="300"
                                  height="300"
                                  style={{
                                    objectFit: "contain",
                                    marginRight: "10px",
                                    display: "block",
                                  }}
                                />

                                <p style={{ fontWeight: "350" }}>
                                  {" "}
                                  {p.product.productName} | Size:{" "}
                                  {p.selectedSize} | Qty: {p.quantity} | Price:{" "}
                                  <h10 style={{ fontWeight: "900px" }}>
                                    ${p.product.productPrice}{" "}
                                  </h10>
                                </p>
                                <hr />
                              </>
                            )}
                          </li>
                        ))}
                      </ol>
                    </div>
                  </div>
                )
            )
          )}
        </div>
      </div>
      <Footer></Footer>
    </>
  );
};

export default AdminOrders;
