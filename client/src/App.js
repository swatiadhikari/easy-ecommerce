import "./App.css";
import React from "react";
import HomePage from "./pages/client/HomePage";
import LoginPage from "./pages/client/LoginPage";
import { Route, Routes } from "react-router-dom";
import SignupPage from "./pages/client/SignupPage";
import WishlistPage from "./pages/client/WishlistPage";
import CartPage from "./pages/client/CartPage";
import AdminLoginPage from "./pages/admin/AdminLoginPage";
import ProductAddPage from "./pages/admin/ProductAddPage";
import SubCategoryAddPage from "./pages/admin/SubCategoryAddPage";
import SubCategoryViewPage from "./pages/admin/SubCategoryViewPage";
import SubCategoryUpdatePage from "./pages/admin/SubCategoryUpdatePage";
import ProductDetailPage from "./pages/client/ProductDetailPage";
import PrivateRoute from "./components/routes/PrivateRoute";
import AdminRoute from "./components/routes/AdminRoute";
import ProductViewPage from "./pages/admin/ProductViewPage";
import ProductUpdatePage from "./pages/admin/ProductUpdatePage";
import CategoryPage from "./pages/client/CategoryPage";
import ProductPage from "./pages/client/ProductPage";
import AdminDashboardPage from "./pages/admin/AdminDashboardPage";
import Search from "./pages/client/SearchProduct";
import UserOrders from "./pages/client/UserOrders";
import Checkout from "./pages/client/Checkout";
import UserProfilePage from "./pages/client/UserProfilePage";
import { Toaster } from "react-hot-toast";
import NotFoundPage from "./pages/client/NotFoundPage";
import AdminOrders from "./pages/admin/Orders";
import PastOrders from "./pages/admin/PastOrders";
import CarouselImagesAddPage from "./pages/admin/CarouselImagesAddPage";
import ReviewForm from "./pages/client/ReviewForm";
import BulkEmail from "./pages/admin/BulkEmail";
import CustomerRoute from "./components/routes/CustomerRoute";

function App() {
  return (
    <>
      <Toaster />
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/signup" element={<SignupPage />} />
        <Route path="/search" element={<Search />} />
        <Route path="/admin" element={<AdminLoginPage />} />
        <Route path="*" element={<NotFoundPage />} />

        {/*Customer Route chamges*/}
        <Route path="/" element={<CustomerRoute />}>
          <Route path="/category/:category" element={<CategoryPage />} />
          <Route index element={<HomePage />} />
          <Route
            path="/:category/products/:subcategory"
            element={<ProductPage />}
          />
          <Route path="/products/:id" element={<ProductDetailPage />} />
        </Route>

        {/*Private routes*/}
        <Route path="/" element={<PrivateRoute />}>
          <Route path="wishlist" element={<WishlistPage />} />
          <Route path="cart" element={<CartPage />} />
          <Route path="cart/checkout" element={<Checkout />} />
          <Route path="/orders" element={<UserOrders />} />
          <Route path="/profile" element={<UserProfilePage />} />
          <Route path="/reviews" element={<ReviewForm />} />
        </Route>

        {/*Admin Routes*/}
        <Route path="/admin" element={<AdminRoute />}>
          <Route path="add" element={<ProductAddPage />} />
          <Route path="subcategory/add" element={<SubCategoryAddPage />} />
          <Route path="subcategory/view" element={<SubCategoryViewPage />} />
          <Route
            path="subcategory/update/:slug"
            element={<SubCategoryUpdatePage />}
          />
          <Route path="/admin/orders" element={<AdminOrders />}></Route>
          <Route path="/admin/past-orders" element={<PastOrders />}></Route>
          <Route path="/admin/send-email" element={<BulkEmail />}></Route>
          <Route
            path="/admin/product/view"
            element={<ProductViewPage />}
          ></Route>
          <Route
            path="product/update/:productId"
            element={<ProductUpdatePage />}
          />
          <Route
            path="/admin/carousel-images"
            element={<CarouselImagesAddPage />}
          ></Route>
          <Route path="dashboard" element={<AdminDashboardPage />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
