import React from "react";
import { ComponentPreview, Previews } from "@react-buddy/ide-toolbox";
import { PaletteTree } from "./palette";
import CategoryPage from "../pages/client/CategoryPage";

const ComponentPreviews = () => {
  return (
    <Previews palette={<PaletteTree />}>
      <ComponentPreview path="/CategoryPage">
        <CategoryPage />
      </ComponentPreview>
    </Previews>
  );
};

export default ComponentPreviews;
