import React, { useState, createContext, useContext, useEffect } from "react";
import axios from "axios";
import { useAuth } from "./auth";

const CartContext = createContext();
const CartProvider = ({ children }) => {
  const [cart, setCart] = useState([]);
  const [auth] = useAuth();

  const userId = auth.currentUser.id;
  useEffect(() => {
    // let existingCart = localStorage.getItem("cart");
    // if (existingCart) {
    //   setCart(JSON.parse(existingCart));
    // } else {
    if (userId) {
      axios
        .get(`http://localhost:4000/api/cart`)
        .then((response) => {
          setCart(response.data);
          localStorage.setItem("cart", JSON.stringify(response.data));
          console.log(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    // }
  }, [userId]);

  useEffect(() => {
    const updateCart = async () => {
      try {
        if (userId) {
          await axios.post("http://localhost:4000/api/cart/update", {
            userId,
            cart,
          });
        }
      } catch (error) {
        console.error(error);
      }
    };
    updateCart();
  }, [cart]);

  const addToCart = (product, selectedSize, quantity, price) => {
    let updatedCart = [...cart];
    const existingIndex = updatedCart.findIndex(
      (item) => item.product === product && item.selectedSize === selectedSize
    );
    if (existingIndex >= 0) {
      updatedCart[existingIndex].quantity += quantity;
    } else {
      updatedCart.push({ product, quantity, selectedSize, price });
    }
    setCart(updatedCart);
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  const incrementItem = (product, size) => {
    let updatedCart = [...cart];
    const existingIndex = updatedCart.findIndex(
      (item) => item.product._id === product._id && item.selectedSize === size
    );
    updatedCart[existingIndex].quantity += 1;
    setCart(updatedCart);
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  const decrementItem = (product, size) => {
    let updatedCart = [...cart];
    const existingIndex = updatedCart.findIndex(
      (item) => item.product === product._id && item.selectedSize === size
    );
    if (updatedCart[existingIndex].quantity > 1) {
      updatedCart[existingIndex].quantity -= 1;
    } else {
      updatedCart.splice(existingIndex, 1);
    }
    setCart(updatedCart);
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  const getTotalQuantity = () => {
    return cart.reduce((total, item) => total + item.quantity, 0);
  };

  const getCartTotal = () => {
    if (!cart || cart.length === 0) {
      return 0;
    }

    return cart.reduce((total, item) => total + item.quantity * item.price, 0);
  };

  const clearCart = () => {
    setCart([]);
    localStorage.removeItem("cart");
  };

  return (
    <CartContext.Provider
      value={{
        cart,
        addToCart,
        getTotalQuantity,
        clearCart,
        getCartTotal,
        incrementItem,
        decrementItem,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

const useCart = () => useContext(CartContext);

export { useCart, CartProvider };
