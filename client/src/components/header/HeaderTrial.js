import React, { useEffect, useState } from "react";
import logo from "../images/logo.png";
import "./Header.css";
import { Link, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import { useAuth } from "../../context/auth";
import { useCart } from "../../context/cart";
import { Badge } from "antd";
import {
  ShoppingCartOutlined,
  HeartOutlined,
  UserOutlined,
} from "@ant-design/icons";

import Search from "../SearchQuery";

const HeaderTrial = () => {
  const [categories, setCategories] = useState([]);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const { getTotalQuantity } = useCart();
  const [auth, setAuth] = useAuth();
  const navigate = useNavigate();
  const location = useLocation();

  const logout = () => {
    setAuth({ ...auth, user: null, token: "" });
    localStorage.clear();
    navigate("/");
  };

  useEffect(() => {
    axios
      .get("http://localhost:4000/api/categories")
      .then((response) => setCategories(response.data))
      .catch((error) => console.log(error));
  }, [auth.currentUser.id]);

  const handleDropdownOpen = () => {
    setIsDropdownOpen(true);
  };

  const handleDropdownClose = () => {
    setIsDropdownOpen(false);
  };

  return (
    <nav className="navbar navbar-expand-lg bg-white border-bottom navbar-toggler">
      <div className="container-fluid">
        <div className="navbar-brand">
          <Link to={"/"}>
            <img
              className="logo img-fluid"
              src={logo}
              alt="Logo"
              width="250"
              height="95"
            />
          </Link>
        </div>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarTogglerDemo03"
          aria-controls="navbarTogglerDemo03"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="d-flex collapse navbar-collapse"
          id="navbarTogglerDemo03"
        >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {categories.map((category) => (
              <li
                className={`nav-item ${
                  location.pathname.includes(`/category/${category.name}`) ||
                  location.pathname.includes(`/${category.name}/products`)
                    ? "active"
                    : ""
                }`}
                key={category.id}
              >
                <Link
                  to={`/category/${category.name}`}
                  className="text-decoration-none text-reset"
                >
                  <a className="nav-link">{category.name}</a>
                </Link>
              </li>
            ))}
          </ul>
          <div className="navbar-collapse justify-content-center ms-auto">
            <Search />
          </div>
          <div
            className="d-flex collapse navbar-collapse"
            id="navbarTogglerDemo03"
          >
            <div className="d-flex ms-auto">
              <div
                className="dropdown"
                onMouseEnter={handleDropdownOpen}
                onMouseLeave={handleDropdownClose}
              >
                <button
                  type="button"
                  className="btn dropdown-toggle"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <UserOutlined style={{ fontSize: "130%" }} />
                  <br />
                  {auth.token ? (
                    <label style={{ fontWeight: "400" }}>
                      {auth.currentUser.name.first}
                    </label>
                  ) : (
                    <label style={{ fontWeight: "400" }}>Profile</label>
                  )}
                </button>
                <ul
                  className={`dropdown-menu ${isDropdownOpen ? "show" : ""}`}
                  aria-labelledby="dropdownMenuButton2"
                >
                  <li>
                    <Link
                      to="/profile"
                      className="dropdown-item"
                      style={{ fontWeight: "400" }}
                    >
                      Profile
                    </Link>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <Link
                      to="/orders"
                      className="dropdown-item"
                      style={{ fontWeight: "400" }}
                    >
                      Orders
                    </Link>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  {!auth.token ? (
                    <li style={{ fontWeight: "400" }}>
                      <Link
                        to="/login"
                        className="dropdown-item"
                        style={{ fontWeight: "400" }}
                      >
                        Login/SignUp
                      </Link>
                    </li>
                  ) : (
                    <li>
                      <Link
                        to="/"
                        className="dropdown-item"
                        onClick={logout}
                        style={{ fontWeight: "400" }}
                      >
                        Logout
                      </Link>
                    </li>
                  )}
                </ul>
              </div>
              <button type="button" className="btn">
                <Link
                  to="/wishlist"
                  className="text-decoration-none text-reset"
                >
                  <div>
                    <HeartOutlined style={{ fontSize: "130%" }} />
                    <br />
                    <label style={{ fontWeight: "400" }}>Wishlist</label>
                  </div>
                </Link>
              </button>
              <button type="button" className="btn">
                <Link to="/cart" className="text-decoration-none text-reset">
                  <div>
                    <Badge count={getTotalQuantity()}>
                      <ShoppingCartOutlined style={{ fontSize: "140%" }} />
                    </Badge>
                    <br />
                    <label style={{ fontWeight: "400" }}>Cart</label>
                  </div>
                </Link>
              </button>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default HeaderTrial;
