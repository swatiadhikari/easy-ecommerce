import React, { useEffect, useState } from "react";
import { useCart } from "../../context/cart";
import { useAuth } from "../../context/auth";
import { Link, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import logo from "../images/logo.png";
import Search from "../SearchQuery";
import {
  HeartOutlined,
  ShoppingCartOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Badge } from "antd";
import "./Header.css";
import { toast } from "react-hot-toast";
const Header = () => {
  const [categories, setCategories] = useState([]);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const { getTotalQuantity } = useCart();
  const [auth, setAuth] = useAuth();
  const navigate = useNavigate();
  const location = useLocation();

  const logout = () => {
    setAuth({ ...auth, user: null, token: "" });
    localStorage.clear();
    navigate("/");
    toast.success("Successful Logout");
  };

  useEffect(() => {
    axios
      .get("http://localhost:4000/api/categories")
      .then((response) => setCategories(response.data))
      .catch((error) => console.log(error));
  }, [auth.currentUser.id]);

  const handleDropdownOpen = () => {
    setIsDropdownOpen(true);
  };

  const handleDropdownClose = () => {
    setIsDropdownOpen(false);
  };
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-white">
        <div className="container-fluid">
          <Link to={"/"}>
            <img
              className="logo img-fluid"
              src={logo}
              alt="Logo"
              width="250"
              height="95"
              style={{ marginRight: "45px" }}
            />
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav mobile">
              {categories.map((category) => (
                <li
                  className={`nav-item ${
                    location.pathname.includes(`/category/${category.name}`) ||
                    location.pathname.includes(`/${category.name}/products`)
                      ? "active"
                      : ""
                  }`}
                  key={category.name}
                >
                  <Link
                    to={`/category/${category.name}`}
                    className="text-decoration-none text-reset "
                  >
                    <span className="nav-link">{category.name}</span>
                  </Link>
                </li>
              ))}
            </ul>
            <div className="navbar-collapse justify-content-center ms-auto">
              <Search />
            </div>
            <div className="dropdown" onMouseLeave={handleDropdownClose}>
              <button
                type="button"
                className="btn dropdown-toggle"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                onMouseEnter={handleDropdownOpen}
              >
                <UserOutlined style={{ fontSize: "130%" }} />
                <br />
                {auth.token ? (
                  <label style={{ fontWeight: "400" }}>
                    {auth.currentUser.name.first}
                  </label>
                ) : (
                  <label style={{ fontWeight: "400" }}>Profile</label>
                )}
              </button>
              <ul
                className={`dropdown-menu ${isDropdownOpen ? "show" : ""}`}
                aria-labelledby="dropdownMenuButton2"
              >
                <li key="profile">
                  <Link
                    to="/profile"
                    className="dropdown-item"
                    style={{ fontWeight: "400" }}
                  >
                    Profile
                  </Link>
                </li>
                <li key="div1">
                  <hr className="dropdown-divider" />
                </li>
                <li key="orders">
                  <Link
                    to="/orders"
                    className="dropdown-item"
                    style={{ fontWeight: "400" }}
                  >
                    Orders
                  </Link>
                </li>
                <li key="div2">
                  <hr className="dropdown-divider" />
                </li>
                {!auth.token ? (
                  <li style={{ fontWeight: "400" }} key="Login">
                    <Link
                      to="/login"
                      className="dropdown-item"
                      style={{ fontWeight: "400" }}
                    >
                      Login/SignUp
                    </Link>
                  </li>
                ) : (
                  <li key="logout">
                    <Link
                      to="/"
                      className="dropdown-item"
                      onClick={logout}
                      style={{ fontWeight: "400" }}
                    >
                      Logout
                    </Link>
                  </li>
                )}
              </ul>
              <button type="button" className="btn">
                <Link
                  to="/wishlist"
                  className="text-decoration-none text-reset"
                >
                  <div>
                    <HeartOutlined style={{ fontSize: "130%" }} />
                    <br />
                    <label style={{ fontWeight: "400" }}>Wishlist</label>
                  </div>
                </Link>
              </button>
              <button type="button" className="btn">
                <Link to="/cart" className="text-decoration-none text-reset">
                  <div>
                    <Badge count={getTotalQuantity()}>
                      <ShoppingCartOutlined style={{ fontSize: "140%" }} />
                    </Badge>
                    <br />
                    <label style={{ fontWeight: "400" }}>Cart</label>
                  </div>
                </Link>
              </button>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Header;
