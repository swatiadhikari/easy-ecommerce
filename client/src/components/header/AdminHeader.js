import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import "./AdminHeader.css";
import { toast } from "react-hot-toast";
import { useAuth } from "../../context/auth";

const AdminHeader = () => {
  const [activeTab, setActiveTab] = useState("");
  const [auth, setAuth] = useAuth();
  const navigate = useNavigate();
  const handleTabClick = (tabName) => {
    setActiveTab(tabName);
  };
  const logout = () => {
    setAuth({ ...auth, user: null, token: "" });
    localStorage.clear();
    navigate("/");
    toast.success("Successful Logout");
  };

  return (
    <header>
      <nav className="navbar navbar-expand-lg bg-white border-bottom">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul
              className="navbar-nav justify-content-center "
              style={{ fontWeight: "350" }}
            >
              <li className="nav-item">
                <NavLink
                  exact
                  to="/admin/dashboard"
                  className={`nav-link ${
                    activeTab === "dashboard" ? "active" : ""
                  }`}
                  onClick={() => handleTabClick("dashboard")}
                >
                  Dashboard
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/admin/orders"
                  className={`nav-link ${
                    activeTab === "orders" ? "active" : ""
                  }`}
                  onClick={() => handleTabClick("orders")}
                >
                  New Orders
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/admin/past-orders"
                  className={`nav-link ${
                    activeTab === "pastOrder" ? "active" : ""
                  }`}
                  onClick={() => handleTabClick("pastOrder")}
                >
                  Past Order
                </NavLink>
              </li>
              {/*<li className="nav-item">*/}
              {/*  <NavLink*/}
              {/*    to="/admin/analytics"*/}
              {/*    className={`nav-link ${*/}
              {/*      activeTab === "analytics" ? "active" : ""*/}
              {/*    }`}*/}
              {/*    onClick={() => handleTabClick("analytics")}*/}
              {/*  >*/}
              {/*    Analytics*/}
              {/*  </NavLink>*/}
              {/*</li>*/}
              <li className="nav-item">
                <NavLink
                  to="/admin/add"
                  className={`nav-link ${
                    activeTab === "addProduct" ? "active" : ""
                  }`}
                  onClick={() => handleTabClick("addProduct")}
                >
                  Add Product
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to="/admin/product/view"
                  className={`nav-link ${
                    activeTab === "viewProduct" ? "active" : ""
                  }`}
                  onClick={() => handleTabClick("viewProduct")}
                >
                  View/Update Product
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to="/admin/subcategory/add"
                  className={`nav-link ${
                    activeTab === "addSubcategory" ? "active" : ""
                  }`}
                  onClick={() => handleTabClick("addSubcategory")}
                >
                  Add Subcategory
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to="/admin/subcategory/view"
                  className={`nav-link ${activeTab === "view" ? "active" : ""}`}
                  onClick={() => handleTabClick("view")}
                >
                  View/Update Subcategory
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to="/admin/carousel-images"
                  className={`nav-link ${activeTab === "view" ? "active" : ""}`}
                  onClick={() => handleTabClick("view")}
                >
                  Carousel Images
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to="/admin/send-email"
                  className={`nav-link ${activeTab === "view" ? "active" : ""}`}
                  onClick={() => handleTabClick("view")}
                >
                  Send Email
                </NavLink>
              </li>
            </ul>
          </div>
          <button className="btn button" onClick={logout}>
            Logout
          </button>
        </div>
      </nav>
    </header>
  );
};

export default AdminHeader;
