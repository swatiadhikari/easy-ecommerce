import React, { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { useAuth } from "../../context/auth";
import Loading from "./Loading";
import axios from "axios";

const CustomerRoute = () => {
  const [auth] = useAuth();
  const [ok, setOk] = useState(true);

  useEffect(() => {
    const customerCheck = async () => {
      const { data } = await axios.get(`/customer-check`);
      console.log(data);
      if (data.ok) {
        setOk(true);
      } else {
        setOk(false);
      }
    };

    if (auth.token) customerCheck();
  }, [auth.token]);

  return ok ? <Outlet /> : <Loading />;
};

export default CustomerRoute;
